# Wanda the fish

[![License: GPL v3+](https://img.shields.io/badge/license-GPL%20v3%2B-blue.svg)](http://www.gnu.org/licenses/gpl-3.0) 
[![Javascript Support](https://img.shields.io/badge/gjs-1.50.x-orange.svg)](https://gitlab.gnome.org/GNOME/gjs/wikis/Home)

*Wanda the fish* is a GNOME shell extension that puts a fish icon in the top bar and giving you fortune when you click on it.

Be sure to have `fortune-mod` installed.

## Installation

You can install the version on this repo just typing these commands in your terminal:

    git clone https://gitlab.com/tallero/wandathefish
    cp -r wandathefish/wandathefish\@arcipelago.org ~/.local/share/gnome-shell/extensions
    gnome-shell-extension-tool -e wandathefish@arcipelago.org

If still not working, restart `gnome-shell` pressing `Alt+F2` and then executing `r`.

## About

This program is licensed under [GNU General Public License v3 or later](https://www.gnu.org/licenses/gpl-3.0.en.html) by [Pellegrino Prevete](http://prevete.ml). If you find this program useful, consider offering me a [beer](https://patreon.com/tallero), a new [computer](https://patreon.com/tallero) or a part time remote [job](mailto:pellegrinoprevete@gmail.com) to help me pay the bills.

Source program for this program comes from [GNOME Extensions website](https://extensions.gnome.org/extension/676/wanda-the-fish/) and its original author is [k_labrie](https://extensions.gnome.org/accounts/profile/k_labrie) has been released as GPLv2.
